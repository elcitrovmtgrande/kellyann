import React from 'react';
import './HomePage.css';
import logo from './logo.png';
// import logo from './logo_ka_bleu.png';

const HomePage = () => (
  <div id="banner">
    <div id="bannerFilter">
      <img src={logo} alt="Algar Dubos" id="banner--img" />
      <h1 id="banner--title">
        L'expertise d'un cabinet de prestige, la flexibilité d'une structure
        indépendante.
      </h1>
    </div>
  </div>
);

export default HomePage;
