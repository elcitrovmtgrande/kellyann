import React from 'react';
import { Link } from 'react-router-dom';
import './MainMenu.css';

const MainMenu = () => (
  <div className="menu">
    <div className="menu-title">ALGAR DUBOS</div>
    <nav className="nav">
      <ul className="ul">
        <li>
          <Link to="/" className="a">
            Accueil
          </Link>
        </li>
        <li>
          <Link to="/expertise" className="a">
            Expertise
          </Link>
        </li>
        <li>
          <Link to="/international" className="a">
            International
          </Link>
        </li>
        {/* <li>
          <Link to="/posts" className="a">
            Publications
          </Link>
        </li> */}
        <li>
          <Link to="/contact" className="a">
            Contact
          </Link>
        </li>
      </ul>
    </nav>
  </div>
);

export { MainMenu };
